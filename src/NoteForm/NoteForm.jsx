import React from "react";
import "./NoteForm.css";

class NoteForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newNoteContent: ""
    };
  }

  handleUserInput = e => {
    this.setState({
      newNoteContent: e.target.value
    });
  };

  writeNote = e => {
    e.preventDefault();
    this.props.addNote(this.state.newNoteContent);
    this.setState({
      newNoteContent: ""
    });
  };
  render() {
    return (
      <div className="formWrapper">
        <input
          type="text"
          className="noteInput"
          placeholder="Write a new note..."
          value={this.state.newNoteContent}
          onChange={this.handleUserInput}
        />
        <button onClick={this.writeNote} className="noteButton">
          Add Note
        </button>
      </div>
    );
  }
}

export default NoteForm;
